def insertionSort(array):
    for step in range(1, len(array)):
        key = array[step]
        i = step - 1
          
        while i >= 0 and key < array[i]:
            array[i + 1] = array[i]
            i = i - 1

        array[i + 1] = key

data = [10, 5, 1, 4, -3]
insertionSort(data)
print('Sorted :')
print(data)